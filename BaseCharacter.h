#ifndef BASE_CHARACTER_H
#define BASE_CHARACTER_H

#include "raylib.h"

// abstract class = it can't be instantiated, only derived from
class BaseCharacter{
    public:
        BaseCharacter();
        Vector2 getWorldPos() { return worldPos; }
        void undoMovement();
        Rectangle getCollisionRec();
        virtual void tick(float deltaTime);

        // a pure virtual function
        // vector used to draw the character relative to the screen (upper left corner from character to u-l corner screen)
        virtual Vector2 getScreenPos() = 0;
        bool getAlive(){ return alive; };
        void setAlive(bool isAlive){ alive = isAlive; };
    protected:
        Texture2D texture = LoadTexture("characters/knight_idle_spritesheet.png");
        Texture2D idle = LoadTexture("characters/knight_idle_spritesheet.png");
        Texture2D run = LoadTexture("characters/knight_run_spritesheet.png");
        // vector used to position the background relative to the screen (upper left corner from both)
        Vector2 worldPos{};
        Vector2 worldPosLastFrame{};
        // 1 : facing right, -1 facing left
        float rightLeft = 1.f;
        // animation variables
        float runningTime{};
        int frame{};
        int maxFrames = 6;
        float updateTime = 1.f / 12.f;
        float speed = 4.f;
        float width{};
        float height{};
        float scale{};
        Vector2 velocity{};
    private:
        bool alive = true;
};

#endif