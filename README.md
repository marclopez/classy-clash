Small game created in C++ with the raylib library, https://itch.io/game-assets/free assets and https://www.mapeditor.org/ tile created.
Part of the "C++ Fundamentals: Game Programming For Beginners" course from GameDev.tv.
