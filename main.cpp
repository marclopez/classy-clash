#include "raylib.h"
#include "raymath.h"
#include "Character.h"
#include "Prop.h"
#include "Enemy.h"
#include <string>

int main()
{
    // window size
    int windowDimensions[2];
    windowDimensions[0] = 384;
    windowDimensions[1] = 384;
    // initialize window
    InitWindow(windowDimensions[0], windowDimensions[1], "Marc's Classy Clash");
    SetTargetFPS(60);

    // background
    Texture2D background = LoadTexture("nature_tileset/WorldMap.png");
    Vector2 bgPos{0.0, 0.0};
    const float mapScale = 4.f;

    // character
    Character knight(windowDimensions[0], windowDimensions[1], mapScale);

    // Rock
    Texture2D rockTexture = LoadTexture("nature_tileset/Rock.png");
    Texture2D logTexture = LoadTexture("nature_tileset/Log.png");
    Prop rock(Vector2{}, rockTexture, mapScale);
    Prop props[2]{
        Prop{Vector2{600.f, 300.f}, rockTexture, mapScale},
        Prop{Vector2{400.f, 500.f}, logTexture, mapScale}};

    // Enemies
    Texture2D goblin_idle = LoadTexture("characters/goblin_idle_spritesheet.png");
    Texture2D goblin_run = LoadTexture("characters/goblin_run_spritesheet.png");
    Enemy goblin{
        Vector2{800.f, 300.f},
        goblin_idle,
        goblin_run,
        mapScale
    };
    Texture2D slime_idle = LoadTexture("characters/slime_idle_spritesheet.png");
    Texture2D slime_run = LoadTexture("characters/slime_run_spritesheet.png");
    Enemy slime{
        Vector2{static_cast<float>(windowDimensions[0]),
                static_cast<float>(windowDimensions[1])},
        slime_idle,
        slime_run,
        mapScale
    };
    Enemy *enemies[]{
        &goblin,
        &slime
    };
    for (auto enemy : enemies)
    {
        enemy->setTarget(&knight);
    }
    int enemiesDead = 0;

    while (!WindowShouldClose())
    {
        // get Delta Time (time since last frame)
        const float dT = GetFrameTime();

        // start drawing
        BeginDrawing();

        // background
        ClearBackground(WHITE);

        // update map position and flip it to a negative value
        bgPos = Vector2Scale(knight.getWorldPos(), -1.f);

        // draw the map
        DrawTextureEx(background, bgPos, 0.0, mapScale, WHITE);

        // character
        knight.tick(dT);

        // check map bounds
        if (knight.getWorldPos().x < 0.f ||
            knight.getWorldPos().y < 0.f ||
            knight.getWorldPos().x + windowDimensions[0] > background.width * mapScale ||
            knight.getWorldPos().y + windowDimensions[1] > background.height * mapScale)
        {
            knight.undoMovement();
        }

        // draw props
        for (auto prop : props)
        {
            prop.Render(knight.getWorldPos());
            if (
                CheckCollisionRecs(
                    knight.getCollisionRec(),
                    prop.getCollisionRec(knight.getWorldPos())))
            {
                knight.undoMovement();
            }
        }
        
        // draw health string
        if (!knight.getAlive())
        {
            // character not alive
            DrawText("Game Over!", 55.f, 45.f, 40, RED);
            EndDrawing();
            continue;
        }
        else if (enemiesDead == sizeof(enemies) / sizeof(int))
        {
            // All enemies dead
            DrawText("You Win!", 55.f, 45.f, 40, WHITE);
            EndDrawing();
            continue;
        }
        else
        {
            // character is alive
            // using string library create a variable
            std::string knightsHealth = "Health: ";
            // using string library appends the health value, converting it to string
            knightsHealth.append(std::to_string(knight.getHealth()), 0, 5);
            // draw text needs a C type string, using string library to convert it
            DrawText(knightsHealth.c_str(), 55.f, 45.f, 40, RED);
        }

        // loop enemies
        for (auto enemy : enemies)
        {
            if (!enemy->getAlive()) continue;

            // draw enemy
            enemy->tick(dT);

            // checking for attack collisions
            if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
            {
                if (CheckCollisionRecs(enemy->getCollisionRec(), knight.getWeaponCollisionRec()))
                {
                    enemy->setAlive(false);
                    enemiesDead++;
                }
            }
        }

        // end drawing
        EndDrawing();
    }

    UnloadTexture(background);
    UnloadTexture(rockTexture);
    UnloadTexture(logTexture);
    UnloadTexture(goblin_idle);
    UnloadTexture(goblin_run);
    UnloadTexture(slime_idle);
    UnloadTexture(slime_run);

    // end window
    CloseWindow();
}