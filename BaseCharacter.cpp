#include "BaseCharacter.h"
#include "raymath.h"

BaseCharacter::BaseCharacter(){}

void BaseCharacter::undoMovement(){
    worldPos = worldPosLastFrame;
}

Rectangle BaseCharacter::getCollisionRec(){
    return Rectangle{
        getScreenPos().x,
        getScreenPos().y,
        width * scale,
        height * scale,
    };
}

/*
* a function that's called every frame
* to update the character's variables
* passing the Delta Time as a param
*/
void BaseCharacter::tick(float deltaTime){
    // setting last world pos
    worldPosLastFrame = worldPos;
   
   // update animation frame
    runningTime += deltaTime;
    if(runningTime >= updateTime){
        frame++;
        runningTime = 0.f;
        if(frame>maxFrames) frame = 0;
    }

    // update the movement
    if(Vector2Length(velocity) != 0.0){
        // set worldPos = worldPos + direction
        worldPos = Vector2Add(worldPos, Vector2Scale(Vector2Normalize(velocity), speed));
        // check directions
        velocity.x < 0.f ? rightLeft = -1.f : rightLeft = 1.f;
        // change texture when running
        texture = run;
    }else{
        texture = idle;
    }
    velocity = {};

    // draw character
    Rectangle source{width * frame, 0.f, rightLeft * width, height};
    Rectangle dest{
        getScreenPos().x,
        getScreenPos().y,
        width * scale,
        height * scale,
    };
    DrawTexturePro(texture, source, dest, Vector2{}, 0.f, WHITE);
}