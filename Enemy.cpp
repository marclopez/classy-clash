#include "Enemy.h"
#include "raymath.h"


Enemy::Enemy(Vector2 pos, Texture2D idle_texture, Texture2D run_texture, float sc){
    worldPos = pos;
    texture = idle_texture;
    idle = idle_texture;
    run = run_texture;
    scale = sc;
    width = texture.width / maxFrames;
    height = texture.height;
    speed = 3.5f;
}

Vector2 Enemy::getScreenPos(){
    return Vector2{
        Vector2Subtract(worldPos, target->getWorldPos())
    };
}

/*
* a function that's called every frame
* to update the enemy's variables
* passing the Delta Time as a param
*/
void Enemy::tick(float deltaTime){
    if(!getAlive()) return;

    // move the enemy towards the target
    velocity = Vector2Subtract(target->getScreenPos(), getScreenPos());
    if(Vector2Length(velocity) < radius) velocity = {};
    BaseCharacter::tick(deltaTime);

    // damage the target
    if(CheckCollisionRecs(target->getCollisionRec(), getCollisionRec())){
        target->takeDamage(damagePerSec * deltaTime);
    }
}